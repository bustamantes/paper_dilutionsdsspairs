#==================================================================================================
#   LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from astropy.cosmology import WMAP9
from scipy.ndimage.filters import gaussian_filter1d
import struct
from itertools import chain
from scipy.interpolate import interp1d
from scipy.optimize import leastsq
import modules.fitool as fitool 
execfile('master.py')


#==================================================================================================
#   LOADING DATA
#==================================================================================================
#Metal estimator ( 0 : KE08,  1: T04 )
ME = 0

data = np.loadtxt( "./data/select_fmr_iso" )
Mass_iso = 10**data[:,2]
Metal_iso = 10**data[:,6+ME]
#Metal_iso = 0.5*(10**data[:,6] + 10**data[:,7])
SFR_iso = 10**data[:,5]
#masking
mask_iso = data[:,6+ME]>0
Mass_iso = Mass_iso[mask_iso]
Metal_iso = Metal_iso[mask_iso]
Metal_iso2 = 10**data[:,6][mask_iso]
SFR_iso = SFR_iso[mask_iso]

ME=0

datapairs = np.loadtxt( "./data/select_fmr_pairs" )
Mass_pair = 10**datapairs[:,2+2]
Metal_pair = 10**datapairs[:,6+2+ME]
SFR_pair = 10**datapairs[:,5+2]
dist_proj = datapairs[:,2]
#masking
mask_pairs = (datapairs[:,6+2+ME]>0)*(dist_proj<10)
Mass_pair = Mass_pair[mask_pairs]
Metal_pair = Metal_pair[mask_pairs]
SFR_pair = SFR_pair[mask_pairs]
dist_proj = dist_proj[mask_pairs]


datapm = np.loadtxt( "./data/select_fmr_pm" )
Mass_pm = 10**datapm[:,2]
Metal_pm = 10**datapm[:,6+ME]
SFR_pm = 10**datapm[:,5]
#masking
mask_pm = datapm[:,6+ME]>0
Mass_pm = Mass_pm[mask_pm]
Metal_pm = Metal_pm[mask_pm]
SFR_pm = SFR_pm[mask_pm]


#==================================================================================================
#   PLOTTING
#==================================================================================================
SFR, Mass, Z_KE08, Z_T04, Counts = np.loadtxt( "./data/FMR_grid.txt").T

mask_filter = (np.isnan(Z_KE08)==False)*Counts>5
result = leastsq(FMR_residuals ,[10,1.0,1.0,1.0,1.0,1.0], 
                  args = (Mass[mask_filter],SFR[mask_filter],10**Z_KE08[mask_filter]), 
                  full_output=True)[0]

result = leastsq(FMR_residuals ,[10,1.0,1.0,1.0,1.0,1.0], args = (Mass_iso,SFR_iso,Metal_iso), full_output=True)[0]

plt.figure( figsize = (12,8) )

plt.subplot(2,2,1)
plt.hist( np.log10(Metal_iso)-FMR_function( Mass_iso, SFR_iso, result ), bins=21, range = (-0.5,0.5), normed = True, alpha = 0.5, color = 'blue', label = 'isolated', lw = 0.0 )
plt.hist( np.log10(Metal_pair)-FMR_function( Mass_pair, SFR_pair, result ), bins=21, range = (-0.5,0.5), normed = True, alpha = 0.5, color = 'red', label = 'pairs', lw = 0.0  )
plt.legend( loc = 'upper left', fancybox = True )

plt.subplot(2,2,2)
plt.hist( np.log10(Metal_iso)-FMR_function( Mass_iso, SFR_iso, result ), bins=15, range = (-0.5,0.5), normed = True, alpha = 0.5, color='blue', label = 'isolated', lw = 0.0 )
plt.hist( np.log10(Metal_pm)-FMR_function( Mass_pm, SFR_pm, result ), bins=15, range = (-0.5,0.5), normed = True, alpha = 0.5, color = 'green', label = 'post-merger', lw = 0.0  )
plt.legend( loc = 'upper left', fancybox = True )

plt.subplot(2,2,3)
Rarray = np.linspace( -0.5, 0.5, 200 )
CD_iso = ECDF(Rarray, np.log10(Metal_iso)-FMR_function( Mass_iso, SFR_iso, result ))
CD_pair = ECDF(Rarray, np.log10(Metal_pair)-FMR_function( Mass_pair, SFR_pair, result ))
CD_pm = ECDF(Rarray, np.log10(Metal_pm)-FMR_function( Mass_pm, SFR_pm, result ))
plt.plot( Rarray, CD_iso, lw = 2, color='blue', label='isolated' )
plt.plot( Rarray, CD_pair, lw = 2, color='red', label='pairs' )
plt.plot( Rarray, CD_pm, lw = 2, color='green', label='post-merger' )
plt.legend( loc = 'lower right', fancybox = True )
plt.xlabel( "Metallicity residual" )

#plt.subplot(2,2,4)
#plt.scatter( np.log10(Metal_pair)-FMR_function( Mass_pair, SFR_pair, result ), dist_proj )
#hist, xr, yr = np.histogram2d( np.log10(Metal_pair)-FMR_function( Mass_pair, SFR_pair, result ), dist_proj, range = ((-0.5, 0.5), (0, 140)) )
#X, Y = np.meshgrid( center(xr), center(yr) )
#plt.contour( X, Y, np.transpose(hist[::,::-1]) )
#plt.xlim( (-0.5, 0.5) )


plt.show()
