#==================================================================================================
#   LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from astropy.cosmology import WMAP9
from scipy.ndimage.filters import gaussian_filter1d
import struct
from itertools import chain
from scipy.interpolate import interp1d
from scipy.optimize import leastsq
import modules.fitool as fitool 
from master import *

#==================================================================================================
#   LOADING DATA
#==================================================================================================
#Loading complete catalogue of SDSS galaxies
data = np.loadtxt( "./data/select_fmr_all" )
z_all = data[:,1]
rp_all = data[:,2]
vp_all = data[:,3]
MassT_all = 10**data[:,4]
MassF_all = 10**data[:,5]
SFRT_all = 10**data[:,6]
SFRF_all = 10**data[:,7]
ZKE08_all = data[:,8]
ZT04_all = data[:,9]
r2_all = data[:,10]
N2_all = data[:,11]
Mu_all = data[:,12]
mask_ZKE08_all = ZKE08_all>0
mask_ZT04_all = ZT04_all>0

"""
#Fitting with raw data
resultKE08 = leastsq(FMR_residuals ,[10,1.0,1.0,1.0,1.0,1.0], 
                 args = (MassT_all[mask_ZKE08_all],SFRT_all[mask_ZKE08_all],10**ZKE08_all[mask_ZKE08_all]), full_output=True)[0]
resultT04 = leastsq(FMR_residuals ,[10,1.0,1.0,1.0,1.0,1.0], 
                 args = (MassT_all[mask_ZT04_all],SFRT_all[mask_ZT04_all],10**ZT04_all[mask_ZT04_all]), full_output=True)[0]


#==================================================================================================
#   PLOTTING
#==================================================================================================
plt.figure( figsize = (10,10) )

#Grid for FMR
Marray = np.linspace( 7, 12, 20 )
SFRarray = np.linspace( -2.5, 2.5, 20 )
Mgrid, SFRgrid = np.meshgrid( Marray, SFRarray )
Zgrid = FMR_function( 10**Mgrid, 10**SFRgrid, resultKE08 )
Zgrid[Zgrid<8] = np.nan

ax = plt.subplot( 1,1,1, projection='3d' )
ax.plot( np.log10(MassT_all[mask_ZKE08_all]), np.log10(SFRT_all[mask_ZKE08_all]), ZKE08_all[mask_ZKE08_all], '.', ms=1, alpha=0.5 )
ax.plot_wireframe( Mgrid, SFRgrid,Zgrid, color='red', linewidth=0.5 )
ax.set_xlim( (8,12) )
ax.set_ylim( (-2.5,2.5) )
ax.set_zlim( (8, 9.4) )

ax.set_xlabel( 'Mass' )
ax.set_ylabel( 'SFR' )
ax.set_zlabel( 'Log(O/H)' )

plt.show()
"""

#Fitting with raw data
resultKE08_SFR_T = leastsq(FMR_residuals ,[10,1.0,1.0,1.0,1.0,1.0], 
                 args = (MassT_all[mask_ZKE08_all],10**ZKE08_all[mask_ZKE08_all], SFRT_all[mask_ZKE08_all]), full_output=True)[0]

plt.figure( figsize = (16,7) )

#Grid for FMR
Marray = np.linspace( 7, 12, 20 )
OHarray = np.linspace( 8, 9.5, 20 )
Mgrid, OHgrid = np.meshgrid( Marray, OHarray )
SFRgrid = FMR_function( 10**Mgrid, 10**OHgrid, resultKE08_SFR_T )
SFRgrid[SFRgrid<-1.5] = np.nan

ax = plt.subplot( 1,2,1, projection='3d' )
ax.plot( np.log10(MassT_all[mask_ZKE08_all]), ZKE08_all[mask_ZKE08_all], np.log10(SFRT_all[mask_ZKE08_all]), '.', ms=1, alpha=0.1 )
ax.plot_wireframe( Mgrid, OHgrid, SFRgrid, color='red', linewidth=0.5 )
ax.view_init(2, 300)
ax.set_xlim( (8,12) )
ax.set_ylim( (8,9.4) )
ax.set_zlim( (-1.5,2) )
ax.set_xlabel( '$\log_{10}(M_{\star}/M_{\odot})$', fontsize = 16 )
ax.set_ylabel( '$\log(O/H) + 12$', fontsize = 16 )
ax.set_zlabel( '$\log_{10}(SFR/M_{\odot}/yr)$', fontsize = 16 )

ax = plt.subplot( 1,2,2, projection='3d' )
ax.plot( np.log10(MassT_all[mask_ZKE08_all]), ZKE08_all[mask_ZKE08_all], np.log10(SFRT_all[mask_ZKE08_all]), '.', ms=1, alpha=0.1 )
ax.plot_wireframe( Mgrid, OHgrid, SFRgrid, color='red', linewidth=0.5 )
#ax.view_init(20, 40)
ax.view_init(2, 45)
ax.set_xlim( (8,12) )
ax.set_ylim( (8,9.4) )
ax.set_zlim( (-1.5,2.) )
ax.set_xlabel( '$\log_{10}(M_{\star}/M_{\odot})$', fontsize = 16 )
ax.set_ylabel( '$\log(O/H) + 12$', fontsize = 16 )
ax.set_zlabel( '$\log_{10}(SFR/M_{\odot}/yr)$', fontsize = 16 )

plt.show()
