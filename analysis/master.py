#==================================================================================================
#   LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from astropy.cosmology import WMAP9
from scipy.ndimage.filters import gaussian_filter1d
import struct
from itertools import chain
from scipy.interpolate import interp1d
from scipy.optimize import leastsq
import modules.fitool as fitool 

#==================================================================================================
#   FUNCTIONS
#==================================================================================================
def FMR_residuals( params, M, SFR, Z ):
    return np.log10(Z) - FMR_function( M, SFR, params )

def Gaussian_fit( params, X, Res ):
    return Res - Gaussian( params, X )

def Gaussian( params, X ):
    sigma = params[0]
    median = params[1]
    return (2*np.pi*sigma**2)**(-0.5)*np.exp( -(X-median)**2/(2*sigma**2) )
    
def FMR_function( M, SFR, params ):
    A = params[0]
    B = params[1]
    C = params[2]
    D = params[3]
    E = params[4]
    F = params[5]
    
    return A + B*(np.log10(M)-10) + C*np.log10(SFR) + D*(np.log10(M)-10)**2 + E*(np.log10(M)-10)*np.log10(SFR) + F*np.log10(SFR)**2

def FMR_function_inv( M, logZ, params ):
    A = params[0]
    B = params[1]
    C = params[2]
    D = params[3]
    E = params[4]
    F = params[5]
    
    return (-( E*(np.log10(M)-10) + C ) - np.sqrt( ( E*(np.log10(M)-10) + C )**2 - 4*F*( D*(np.log10(M)-10)**2 + B*(np.log10(M)-10) + A - logZ ) ) )/(2*F)


def Percentiles( X, Y, Nbins=10, xmin='auto', xmax=None, percentiles = [50,] ):
    if xmin == 'auto':
        xmin = X.min()
        xmax = X.max()
    delta = (xmax-xmin)*1.0/Nbins
    Xbins = np.linspace( xmin, xmax, Nbins+1 ) + 0.5*delta
    Xbins = Xbins[:-1]
    data_perc = []
    for i in xrange(Nbins):
        try:
            data_perc.append( np.percentile(Y[ (X>=xmin+delta*i)*(X<xmin+delta*(i+1)) ], percentiles) )
        except:
            data_perc.append( np.zeros(len(percentiles)) )
    data_perc = np.array(data_perc)
    
    return Xbins, data_perc

def ECDF(X, data):
    dist = []
    for x in X:
        dist.append(sum(data<=x))
    return np.array(dist)*1.0/len(data)

def center(X):
    #return X[:-1]
    return 0.5*(X[:-1]+X[1:])
