#==================================================================================================
#   LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from astropy.cosmology import WMAP9
from scipy.ndimage.filters import gaussian_filter1d
import struct
from itertools import chain
from scipy.interpolate import interp1d
from scipy.optimize import leastsq
import modules.fitool as fitool 
execfile('master.py')


#==================================================================================================
#   LOADING DATA
#==================================================================================================
data = np.loadtxt( "./data/select_fmr_iso" )
Mass_iso = 10**data[:,2]
Metal_iso = 10**data[:,6]
SFR_iso = 10**data[:,5]

datapairs = np.loadtxt( "./data/select_fmr_pairs" )
Mass_pair = 10**datapairs[:,2+2]
Metal_pair = datapairs[:,7+2]
SFR_pair = 10**datapairs[:,4+2]

datapm = np.loadtxt( "./data/select_fmr_pm" )
Mass_pm = 10**datapm[:,2]
Metal_pm = datapm[:,7]
SFR_pm = 10**datapm[:,4]


#==================================================================================================
#   PLOTTING
#==================================================================================================
result = leastsq(FMR_residuals ,[10,1.0,1.0,1.0,1.0,1.0], args = (Mass_iso,SFR_iso,Metal_iso), full_output=True)[0]

Mass_array = np.linspace( 7, 11.5, 20 )
SFR_array = np.linspace( -2, 2, 20 )
MassGrid, SFRGrid = np.meshgrid( Mass_array, SFR_array )

xbins, percentiles = Percentiles( np.log10(Mass_iso),np.log10(Metal_iso), Nbins=15, percentiles = [5,16,50,84,90] )

#plt.plot( np.log10(Mass_iso),np.log10(Metal_iso), '.', ms = 0.2, color='black' )

plt.fill_between( xbins, percentiles[:,0], percentiles[:,4], color='gray', alpha = 0.3 )
plt.fill_between( xbins, percentiles[:,1], percentiles[:,3], color='gray', alpha = 0.7 )
plt.plot( xbins, percentiles[:,0], '-', color='black', lw=1 )
plt.plot( xbins, percentiles[:,1], '-', color='black', lw=1 )
plt.plot( xbins, percentiles[:,2], '-', color='black', lw=2 )
plt.plot( xbins, percentiles[:,3], '-', color='black', lw=1 )
plt.plot( xbins, percentiles[:,4], '-', color='black', lw=1 )

#SFR range
SFR = np.arange( -1.45,0.8,0.15 )
for sfr_i, sfr_ip, i in zip( SFR[:-1], SFR[1:], np.linspace(0.3,1,len(SFR)) ):
    mask_sfr = (np.log10(SFR_iso)>=sfr_i)*(np.log10(SFR_iso)<sfr_ip)
    xbins_sfr, percentiles_sfr = Percentiles( np.log10(Mass_iso[mask_sfr]),np.log10(Metal_iso[mask_sfr ]), Nbins=10, percentiles = [50] )
    #plt.plot( xbins_sfr, percentiles_sfr[:,0], '-', lw=3, alpha = i, color='red' )

plt.xlim( (8.5,11.5) )
plt.ylim( (8.2,9.2) )
plt.show()
